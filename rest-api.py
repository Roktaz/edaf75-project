from flask import Flask, jsonify, abort, request, make_response, url_for
from flask_restful import Api, Resource, reqparse
from dbapi import *

app = Flask(__name__)
api = Api(app)
conn = connect_db()


@app.route('/reset', methods=['POST'])
def reset_db():
    reset(conn)
    return jsonify({'status': 'ok'})


@app.route('/customers', methods=['GET'])
def get_customers():
    result = query(conn, 'SELECT * FROM customers ORDER BY name')
    return jsonify({'customers': result})


@app.route('/cookies', methods=['GET'])
def get_movie():
    result = query(
        conn, 'SELECT cookie_name FROM cookies ORDER BY cookie_name')
    return jsonify({'cookies': result})


@app.route('/ingredients', methods=['GET'])
def get_ingredients():
    result = query(conn, 'SELECT * FROM ingredients')
    return jsonify({'ingredients': result})


@app.route('/pallets', methods=['POST'])
def post_pallets():
    cookie = request.args.get('cookie', None)
    cur = conn.cursor()
    cur.execute('SELECT * FROM recipes WHERE cookie_name = (?)', [cookie])
    recipe = cur.fetchall()
    if not recipe:
        return jsonify({'status': 'no such cookie'})
    try:
        for ingredient in recipe:
            print(ingredient)
            q = ingredient[2] * 54
            n = ingredient[0]
            cur.execute(
                """
                UPDATE ingredients
                SET quantity = quantity - (?)
                WHERE ingredient_name = (?)
                """, [q, n])

        cur.execute(
            """
            INSERT INTO pallets (cookie_name)
            VALUES (?)
            """, [cookie])
        conn.commit()
        result = query(conn,
                       """
            SELECT pallet_id
            FROM pallets
            WHERE rowid = last_insert_rowid()
            """)
        return jsonify({'status': 'ok',
                        'id': result[0]['pallet_id']})
    except Exception as e:
        print(format(e))
        conn.rollback()
        return jsonify({'status': 'not enough ingredients'})


@app.route('/pallets', methods=['GET'])
def get_pallets():
    cookie = request.args.get('cookie', None)
    after = request.args.get('after', '0000-01-01')
    before = request.args.get('before', '9999-12-12')
    blocked = request.args.get('blocked', '0')

    if cookie is not None:
        result = query(conn, """
        SELECT * 
        FROM pallets
        WHERE cookie_name = (?)
        AND production_date BETWEEN (?) AND (?)
        AND blocked = (?)
        """, [cookie, after, before, blocked])
    else:
        result = query(conn, """
        SELECT * 
        FROM pallets
        WHERE production_date BETWEEN (?) AND (?)
        AND blocked = (?)
        """, [after, before, blocked])
    return jsonify({'pallets': result})


@app.route('/block/<cookie>/<frm>/<to>', methods=['POST'])
def block(cookie, frm, to):
    print(frm)
    query(conn, """
        UPDATE pallets
        SET blocked = 1
        WHERE cookie_name = (?)
        AND production_date BETWEEN (?) AND (?)
        """, [cookie, frm, to])
    return jsonify({'status': 'ok'})


@app.route('/unblock/<cookie>/<frm>/<to>', methods=['POST'])
def unblock(cookie, frm, to):
    query(conn, """
        UPDATE pallets
        SET blocked = 0
        WHERE cookie_name = (?)
        AND production_date BETWEEN (?) AND (?)
        """, [cookie, frm, to])
    return jsonify({'status': 'ok'})


if __name__ == "__main__":
    app.run(debug=True, host='localhost', port=8888)
