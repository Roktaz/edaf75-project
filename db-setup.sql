PRAGMA foreign_keys = OFF;
DROP TABLE IF EXISTS ingredients;
DROP TABLE IF EXISTS recipes;
DROP TABLE IF EXISTS cookies;
DROP TABLE IF EXISTS pallets;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS order_details;
DROP TABLE IF EXISTS customers;
DROP TABLE IF EXISTS deliveries;
PRAGMA foreign_keys=ON;

--Create tables

CREATE TABLE ingredients (
  ingredient_name TEXT,
  quantity REAL CHECK(quantity > 0),
  unit TEXT,
  last_shipment TIMESTAMP,
  PRIMARY KEY (ingredient_name)
);

CREATE TABLE recipes(
  ingredient_name TEXT,
  cookie_name TEXT,
  quantity REAL CHECK(quantity > 0),
  unit TEXT,
  PRIMARY KEY (ingredient_name, cookie_name),
  FOREIGN KEY (ingredient_name) REFERENCES ingredients(name),
  FOREIGN KEY (cookie_name) REFERENCES cookies(cookie_name)
);

CREATE TABLE cookies(
  cookie_name TEXT,
  description TEXT,
  PRIMARY KEY (cookie_name)
);

CREATE TABLE pallets(
  pallet_id TEXT DEFAULT(lower(hex(randomblob(16)))),
  delivery_id TEXT,
  cookie_name TEXT,
  location TEXT,
  blocked BIT DEFAULT(0),
  production_date DATE DEFAULT(CURRENT_DATE),
  PRIMARY KEY (pallet_id),
  FOREIGN KEY (delivery_id) REFERENCES deliveries(delivery_id),
  FOREIGN KEY (cookie_name) REFERENCES cookies(cookie_name)
);

CREATE TABLE orders(
  order_id TEXT DEFAULT(lower(hex(randomblob(16)))),
  customer_id TEXT,
  status TEXT,
  ordered_time TIMESTAMP,
  to_be_delivered DATE,
  PRIMARY KEY (order_id),
  FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);

CREATE TABLE order_details(
  order_id TEXT,
  cookie_id TEXT,
  quantity INT,
  PRIMARY KEY (order_id, cookie_id),
  FOREIGN KEY (order_id) REFERENCES orders(order_id),
  FOREIGN KEY (cookie_id) REFERENCES cookies(cookie_id)
);

CREATE TABLE customers(
  customer_id TEXT DEFAULT(lower(hex(randomblob(16)))),
  name TEXT,
  address TEXT,
  PRIMARY KEY (customer_id)

);

CREATE TABLE deliveries(
  delivery_id TEXT DEFAULT(lower(hex(randomblob(16)))),
  time TIMESTAMP,
  PRIMARY KEY (delivery_id)
);
