# EDAF75, project report

This is the report for

 + Jesper Berg , `elt14jbe`
 + Erik Gralén , `dat15egr`

We solved this project on our own, except for:

 + The Peer-review meeting
 + Code snippets from lab 3


## ER-design

The model is in the file [`er-model.png`](er-model.png):

<center>
    <img src="er-model.png" width="100%">
</center>

## Relations

The ER-model above gives the following relations (neither
[Markdown](https://docs.gitlab.com/ee/user/markdown.html)
nor [HTML5](https://en.wikipedia.org/wiki/HTML5) handles
underlining withtout resorting to
[CSS](https://en.wikipedia.org/wiki/Cascading_Style_Sheets),
so we use bold face for primary keys, italicized face for
foreign keys, and bold italicized face for attributes which
are both primary keys and foreign keys):

+ ingredients(**ingredient_name**, quantity, unit, last_shipment)
+ recipes(**_ingredient_name_**, **_cookie_name_**, quantity, unit)
+ cookies(**cookie_name**, description)
+ pallets(**pallet_id**, _delivery_id_, _cookie_name_, location, blocked, production_date)
+ orders(**order_id** _customer_id_, status, ordered_time, to_be_delivered)
+ order_details(**_order_id_**, **_cookie_id_**, quantity)
+ customers(**customer_id**, name, address)
+ deliveries(**delivery_id**, time)

(this should be replaced with your own relations, of course,
but use the same way of marking primary keys and foreign
keys).


## Scripts to set up database

The scripts used to set up and populate the database are in:

 + [`db-setup.sql`](db-setup.sql) (defines the tables), and
 + Reset function in dbapi.py (inserts data).

So, to create and initialize the database, we run:

```shell
sqlite3 database.db < db-setup.sql
python -c 'from dbapi import *; reset(connect_db())'
```

## How to compile and run the program

This section should give a few simple commands to type to
compile and run the program from the command line, such as:

```shell
python rest-api.py
```
