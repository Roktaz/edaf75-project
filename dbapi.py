import sqlite3
import hashlib
import json


def connect_db():
    print("Connecting to databse")
    conn = sqlite3.connect('database.db', check_same_thread=False)
    return conn


def reset(conn):
    tables = [('cookies'), ('customers'), ('deliveries'), ('ingredients'),
              ('order_details'), ('orders'), ('pallets'), ('recipes')]

    customers = [('Finkakor AB', 'Helsingborg'),
                 ('Småbröd AB', 'Malmö'),
                 ('Kaffebröd AB', 'Landskrona'),
                 ('Bjudkakor AB', 'Ystad'),
                 ('Kalaskakor AB', 'Trelleborg'),
                 ('Partykakor AB', 'Kristianstad'),
                 ('Gästkakor AB', 'Hässleholm'),
                 ('Skånekakor AB', 'Perstorp')
                 ]

    cookies = [('Nut ring', 'This is a great cookie!'),
               ('Nut cookie', 'This is a great cookie!'),
               ('Amneris', 'This is a great cookie!'),
               ('Tango', 'This is a great cookie!'),
               ('Almond delight' ,'This is a great cookie!'),
               ('Berliner' ,'This is a great cookie!')]

    ingredients = [('Flour', 100000.0, 'g'),
                   ('Butter', 100000.0, 'g'),
                   ('Icing sugar', 100000.0, 'g'),
                   ('Roasted, chopped nuts', 100000.0, 'g'),
                   ('Fine-ground nuts', 100000.0, 'g'),
                   ('Ground, roasted nuts', 100000.0, 'g'),
                   ('Bread crumbs', 100000.0, 'g'),
                   ('Sugar', 100000.0, 'g'),
                   ('Egg whites', 100000.0, 'ml'),
                   ('Chocolate', 100000.0, 'g'),
                   ('Marzipan', 100000.0, 'g'),
                   ('Eggs', 100000.0, 'g'),
                   ('Potato starch', 100000.0, 'g'),
                   ('Wheat flour', 100000.0, 'g'),
                   ('Sodium bicarbonate', 100000.0, 'g'),
                   ('Vanilla', 100000.0, 'g'),
                   ('Chopped almonds', 100000.0, 'g'),
                   ('Cinnamon', 100000.0, 'g'),
                   ('Vanilla sugar', 100000.0, 'g')]

    recipes = [('Nut ring', 'Flour', 450),
                ('Nut ring', 'Butter', 450),
                ('Nut ring', 'Icing sugar', 190),
                ('Nut ring', 'Roasted, chopped nuts', 225),
                ('Nut cookie', 'Fine-ground nuts', 750),
                ('Nut cookie', 'Ground, roasted nuts', 625),
                ('Nut cookie', 'Bread crumbs', 125),
                ('Nut cookie', 'Sugar', 375),
                ('Nut cookie', 'Egg whites', 350),
                ('Nut cookie', 'Chocolate', 50),
                ('Amneris', 'Marzipan', 750),
                ('Amneris', 'Butter', 250),
                ('Amneris', 'Eggs', 250),
                ('Amneris', 'Potato starch', 25),
                ('Amneris', 'Wheat flour', 25),
                ('Tango', 'Butter', 200),
                ('Tango', 'Sugar', 250),
                ('Tango', 'Flour', 300),
                ('Tango', 'Sodium bicarbonate', 4),
                ('Tango', 'Vanilla', 2),
                ('Almond delight', 'Butter', 400),
                ('Almond delight', 'Sugar', 270),
                ('Almond delight', 'Chopped almonds', 279),
                ('Almond delight', 'Flour', 400),
                ('Almond delight', 'Cinnamon', 10),
                ('Berliner', 'Flour', 350),
                ('Berliner', 'Butter', 250),
                ('Berliner', 'Icing sugar', 100),
                ('Berliner', 'Eggs', 50),
                ('Berliner', 'Vanilla sugar', 5),
                ('Berliner', 'Chocolate', 50)]

    cur=conn.cursor()
    for x in tables:
        cur.execute('DELETE FROM ' + x)
    cur.executemany(
        'INSERT OR REPLACE INTO customers (name, address) VALUES (?,?)', customers)
    cur.executemany(
        'INSERT OR REPLACE INTO cookies(cookie_name,description) VALUES (?,?)' , cookies)
    cur.executemany(
        'INSERT OR REPLACE INTO ingredients (ingredient_name, quantity, unit) VALUES (?,?,?)', ingredients)
    cur.executemany(
        'INSERT OR REPLACE INTO recipes (cookie_name, ingredient_name, quantity) VALUES (?,?,?)', recipes)
    conn.commit()


# returns json
def query(conn, *args):
    cur=conn.cursor()
    cur.execute(*args)
    responses=cur.fetchall()
    # Empty lists are false
    if(not responses):
        return "{}"
    headers=[i[0] for i in cur.description]
    data=[]
    for r in responses:
        data.append(dict(zip(headers, r)))
    conn.commit()
    return data


def hash(msg):
    return hashlib.sha256(msg.encode('utf-8')).hexdigest()
